# WEB DEVELOPMENT SHELL CONFIGURATOR #

## What it does
* Configures the environment variables `$http_proxy` and `$https_proxy` with your username and password, so command line tools such as `git` and `composer` can download things.
* Improves the command line interface by enabling color in both `bash` and `git`.
* Sets up `git` with user's name and email address and good defaults.
* Sets the command line editor to `nano` because it's easier to use than `vim`.
* Adds lots of bash aliases to make it easier for people used to Windows or MS-DOS.
* Sets up XAMPP's `php` as the main `php` to use from the command line, as Mac's one is often misconfigured.
* Installs `composer` and `laravel` command line tools.

## Using this script (on a Mac)

1. Copy the contents of [web-dev-shell-configurator.sh](https://bitbucket.org/yoobeewebwgtn/web-dev-configurator/src/master/web-dev-shell-configurator.sh) to the clipboard
2. Paste the contents into a new file and save it as `webdev.sh` in your home directory 
3. Open the Terminal application
4. Type into the Terminal window: 
    ```
    chmod u+x webdev.sh
    ```
    This will make the shell script executable
5. Type into the Terminal window:
    ```
    ./webdev.sh
    ```
6. Read and follow instructions on the screen. Watch for potential errors. 

* You may be asked for your Yoobee username and password to get through the proxy.
* You may be asked for your GitHub credentials (username and password), have them ready.

### Student's Personal Laptops

If you are running this script on your own student laptop, the script will set up the command `yoobeeproxy`, which you will need to run before any command line tools will be able to access the internet.

## Shell Commands

* `attrib` - alias to `chmod`
* `chdir` - alias to `cd`
* `copy` - alias to `cp`
* `d` - alias to `dir`
* `del` - alias to `rm`
* `deltree` - alias to `rm -rf`
* `edit` - alias to `$EDITOR` (whatever editor is set by default. this script sets `nano`)
* `l.` - alias to `ls -d .*`
* `ll` - alias to `ls -la`
* `mem` - alias to `top`
* `move` - alias to `mv`
* `search` - alias to `grep`
* `vi` - alias to `vim`
* `pico` - alias to `nano`
* `nano` - alias to `nano -w`


* `showfiles` -- restarts Finder, revealing hidden files and folders
* `hidefiles` -- restarts Finder, concealing hidden files and folders

* `git lg` - one line log, git alias `git log --oneline --decorate --all --graph"
* `git s` - git alias to `git status -s`


* `subl` -- starts Sublime Text from the command line. `subl .` will open the present working folder in Sublime Edit.


* `composer` -- install and manage PHP libraries and frameworks
* `laravel` -- create new Laravel projects

## `git` Configuration
This script will run these `git config` commands:

* `git config --global push.default simple`
* `git config --global branch.autosetuprebase always`
* `git config --global core.autocrlf input`
* `git config --global color.ui true`

as well as setting up http proxy settings with necessary values.

* `git config --global http.proxy http://un:pw@proxy:port`
* `git config --global https.proxy http://un:pw@proxy:port`
