#!/usr/bin/env bash
clear

echo -en "\x1B[35mY\x1B[96mO\x1B[34mO\x1B[91mB\x1B[92mE\x1B[93mE\x1B[39m" # YOOBEE
echo       " WEB DEVELOPMENT SHELL CONFIGURATOR wgtn"
echo "---------------------------------------------------"

if [[ "$1" == "reset" ]] ; then
  rm -rf ~/.composer ~/bin ~/.bash_profile
  git config --global --unset http.proxy
  git config --global --unset https.proxy
	if [[ ! -z $WINDIR ]]; then
		rm -rf ~/AppData/Roaming/Composer/vendor/bin
	fi
	echo -e "\x1B[41;97m\n*****************************************************************"
	echo -e              "*    RESET SUCCESSFUL, CLOSE ALL YOUR TERMINAL WINDOWS, NOW!    *"
	echo -e              "*****************************************************************\x1B[0;m\n"

  exit
fi

### CONSTANTS

if [[ -z $(which git) ]] ; then
	echo -en "\x1B[41;97m"
	echo -e  "FATAL: git not found on this system."
	echo -en "\x1B[0m\n"
	exit
fi

# gitname is the configured username for git, if provided
gitname=$(git config --global --get user.name)

profile_script=$HOME/.bash_profile

touch $profile_script
chmod 700 $profile_script

profile_script=$HOME/.bash_profile

#make the sites directory
mkdir ~/Sites 2> /dev/null

#proxy_host is the url and port number to Yoobee's proxy
proxy_host=proxy:3128

# niceuser is the logged in username, split on fullstop, and the first letters capitalised.
niceuser=$(echo $USER | awk -F'.' '{for (i=1;i<=NF;i++){sub(".",substr(toupper($i),1,1),$i)} print}')

# name is gitname, or if that's blank, niceuser.
name=${gitname:-$niceuser}

# test_url is a page that is on the internet we can use to see if we're online
test_url=http://www.apple.com/library/test/success.html

#
curl_cmd="curl -s -w %{http_code} -L -m 2 --connect-timeout 2 -o /dev/null"
# curl 					# download a file
# -s 					# silence progress, errors
# -w "%{http_code}" 	# output only the http status code
# -L 					# following redirections
# -m 2 					# don't take more than 2 seconds to do it
# --connect-timeout 2 	# if you can't connect in 2 seconds, abort
# -o /dev/null 			# and turf any output you generate

test_cmd="$curl_cmd $test_url"

profile_uncommented=1

### FUNCTIONS

function add_comment_to_profile {
	if [[ $profile_uncommented -eq 1 ]] ; then
		profile_uncommented=0
		echo -e "\n\n#\n# Yoobee Changes $(date)\n#" >> $profile_script
	fi
}

echo ""
echo "   Welcome, $name!"
echo ""
echo "   This shell script configures your terminal session to be able to connect"
echo "   to the internet."
echo "   It also tells git who you are, and configures it to use the proxy."
echo ""
echo -e "   \x1B[91mIMPORTANT\x1B[39m: Your proxy password will be stored in \x1B[91mPLAIN-TEXT\x1B[39m in"
echo "   $profile_script and $HOME/.gitconfig"
echo ""
echo "   We'll also set up Composer, PHP's dependancy manager, "
echo "   and test install Laravel Installer."
echo ""
echo "   http://git-scm.com/   https://getcomposer.org/   http://laravel.com/"
echo ""
echo "-------------------------------------------------------"
echo "   - Press Control+C to cancel this script at any time."

read -p "   - Press [Return] to continue..." key

echo -e "-------------------------------------------------------"
echo -en "\n-> Connecting to the internet..."

statuscode=$($test_cmd)

if [[ $statuscode -eq 200 ]]; then
	echo -e " online!"
else
	while true
	do
		echo -e "\n-> Command Line internet access via Yoobee's HTTP Proxy"
		read -p "   Yoobee Proxy Username: [$USER] " proxy_username
		proxy_username=${proxy_username:-$USER}
		echo "(just press enter to skip command line internet access)"
		read -s -p "   Yoobee Proxy Password: " proxy_password
		echo -e "\n";
		if [[ -z $proxy_password ]] ; then
			echo "...Skipping command line internet access."
			break
		else
			statuscode=$($test_cmd --proxy http://$proxy_username:$proxy_password@$proxy_host)
			if [[ $statuscode -eq 200 ]] ; then
				echo -e "Got online!\n"

				# assume if internet is coming through ethernet port, this is a Yoobee computer
				onLAN=$(ifconfig en0 | grep inet)
				if [[ -z $onLAN ]] ; then
					read -p "Is this computer permanently on the student network at Yoobee? (y/N) " yoobee_lab
				else
					yoobee_lab=Y
				fi
				yoobee_lab=${yoobee_lab:-"n"}
				case $yoobee_lab in
					[Yy]* )
						echo "-> At Yoobee permanently."
						echo "-> Saving proxy setup and password to $profile_script"
						add_comment_to_profile
						echo "export http_proxy=http://$proxy_username:$proxy_password@$proxy_host" >> $profile_script
						echo "export https_proxy=http://$proxy_username:$proxy_password@$proxy_host" >> $profile_script
						;;
					[Nn]* )
						echo "-> At Yoobee sometimes."
						echo "-> Saving proxy setup and password to $profile_script"
						add_comment_to_profile
						echo "alias yoobeeproxy='export http_proxy=http://$proxy_username:$proxy_password@$proxy_host; export https_proxy=http://$proxy_username:$proxy_password@$proxy_host'" >> $profile_script
						export http_proxy=http://$proxy_username:$proxy_password@$proxy_host; export https_proxy=http://$proxy_username:$proxy_password@$proxy_host
						;;
				esac
				break # while
			fi
			echo -e "-> Couldn't get online:\n $statuscode\n -> Was your password correct?"
		fi
	done
fi

echo -e "\n-------------------------------------------------------"

if [[ -z $CLICOLOR ]] ; then
	echo -e "\n-> Enabling CLICOLOR on command line"

	add_comment_to_profile
	echo "export CLICOLOR=1 # enable colours on command line " >> $profile_script
fi


if [[ -z $(alias -p attrib) ]]; then
	echo -e "\n-> Adding many helpful command aliases to $profile_script"
	add_comment_to_profile
	echo "alias attrib='chmod'" >> $profile_script
	echo "alias chdir='cd'" >> $profile_script
	echo "alias copy='cp'" >> $profile_script
	echo "alias d='dir'" >> $profile_script
	echo "alias del='rm'" >> $profile_script
	echo "alias deltree='rm -r'" >> $profile_script
	echo "alias edit='\$EDITOR'" >> $profile_script
	echo "alias l.='ls -d .*'" >> $profile_script
	echo "alias ll='ls -la'" >> $profile_script
	echo "alias mem='top'" >> $profile_script
	echo "alias move='mv'" >> $profile_script
	echo "alias search='grep'" >> $profile_script
	echo "alias vi='vim'" >> $profile_script
	if [[ -z $WINDIR ]]; then
		echo "alias pico='nano'" >> $profile_script
		echo "alias nano='nano -w'" >> $profile_script
		echo "alias showfiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder'" >> $profile_script
		echo "alias hidefiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder'" >> $profile_script
	fi
fi

if [[ ! -d $HOME/bin ]]; then
	echo -e "\n-> Creating ~/bin"
	mkdir -p $HOME/bin
fi

if [[ ":$PATH:" != *":$HOME/bin:"* ]] ; then
	echo -e "\n-> Adding ~/bin to your PATH"
	add_comment_to_profile
	echo "export PATH=~/bin:\$PATH # include user's command line binaries, including composer" >> $profile_script
fi

## SET UP SUBLIME EDITOR AS CLI EDITOR

if [[ -z $WINDIR ]]; then
	if [[ ! -z $(which subl) ]] ; then
		echo -en "\n-> Sublime Edit already set up on command line.\n"

	else
		# places where subl might be found
		subl_locations=(
			/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl
			~/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl
			/Applications/Sublime\ Text\ 2.app/Contents/SharedSupport/bin/subl
			~/Applications/Sublime\ Text\ 2.app/Contents/SharedSupport/bin/subl
		)

		for i in "${subl_locations[@]}" ; do
			if [[ -a $i ]] && [[ ! -f ~/bin/subl ]] ; then
				sublime_location=$i
				echo -e "\n-> Enabling Sublime Edit on command line."
				echo -e "\n    found subl at: "
				echo -e "  > $sublime_location"
				ln -s "$sublime_location" ~/bin/subl
				break
			fi
		done
	fi
fi

if [[ -z $WINDIR ]] && [[ -z $EDITOR ]] ; then
	echo -e "\n-> Setting default EDITOR on command line to nano."
	echo -e "\n\x1B[0;32m    Why not take a look at $profile_script and maybe choose another option?\x1B[0m\n"
	add_comment_to_profile
	echo " " >> $profile_script
	echo "# comment/uncomment out the CLI editor you want git and other tools to invoke" >> $profile_script
	echo "export EDITOR='nano'     # nano, very user friendly" >> $profile_script
	echo "#export EDITOR='vim'     # vim, powerful, default on most systems, very different from most editors. Learn with 'vimtutor', quit with ESC, :q!" >> $profile_script
	echo "#export EDITOR='emacs'   # emacs, powerful, complicated. learn with ^h t, quit with ^x^c" >> $profile_script
	if [[ -z $sublime_location ]]; then
		echo "#export EDITOR='subl -w' # use Sublime Editor. To continue, close the file when you're done" >> $profile_script
	fi
	echo " " >> $profile_script
	echo "export VISUAL='\$EDITOR'" >> $profile_script
	echo "export PAGER=less" >> $profile_script
	echo " " >> $profile_script
fi

### GIT

echo -e  "-------------------------------------------------------"
echo -en "\n-> Setting up Git. "; git --version

echo -e "\n-> Your full name, to mark your commits with (e.g. Joe Bloggs): "
echo "   (press Enter if default is correct)"
read -p "   Full Name [$name]: " full_name

full_name=${full_name:-$name}
git config --global user.name "$full_name"

while [[ -z $email_address ]]
do
	echo -e "\n-> Your GitHub/BitBucket email address (e.g. example@example.com): "
	email_address_old=$(git config --global --get user.email)
	read -p "   Email Address [$email_address_old]: " email_address
	email_address=${email_address:-$email_address_old}
done

git config --global user.email "$email_address"

git config --global push.default simple
git config --global branch.autosetuprebase always
git config --global core.autocrlf input
git config --global color.ui true

git config --global alias.lg "log --oneline --decorate --all --graph"
git config --global alias.s "status -s"

if [[ ! -z $proxy_password ]] && [[ $yoobee_lab == [Yy] ]] ; then
	echo -e "\n-> Permanently saving proxy setup and password to your .gitconfig"

	git config --global http.proxy http://$proxy_username:$proxy_password@$proxy_host
	git config --global https.proxy http://$proxy_username:$proxy_password@$proxy_host
fi


## USE XAMPP PHP, if available

if [[ -d /Applications/XAMPP/bin ]] &&  [[ ":$PATH:" != *":/Applications/XAMPP/bin:"* ]] ; then
	echo -e  "\n-------------------------------------------------------"
	echo -e "\n-> Found XAMPP, adding /Applications/XAMPP/bin to your PATH"
	add_comment_to_profile
	echo "export PATH=/Applications/XAMPP/bin:\$PATH # use XAMPP binaries first (esp. php)" >> $profile_script
fi

if [[ -d /c/xampp/php ]] && [[ ":$PATH:" != *":/c/xampp/bin:"* ]] ; then
	echo -e  "\n-------------------------------------------------------"
	echo -e "\n-> Found XAMPP, adding /c/xampp/php to your PATH"
	add_comment_to_profile
	echo "export PATH=/c/xampp/php:\$PATH # use XAMPP binaries first (esp. php)" >> $profile_script
fi


source $profile_script


if [[ -z $WINDIR ]] && [[ ":$PATH:" != *":$HOME/.composer/vendor/bin:"* ]] ; then
	echo -e "\n-> Adding Composer's ~/.composer/vendor/bin to your PATH"
	add_comment_to_profile
	echo "export PATH=\$HOME/.composer/vendor/bin:\$PATH # include Composer global binaries" >> $profile_script
elif [[ ! -z $WINDIR ]] && [[ ":$PATH:" != *":~/AppData/Roaming/Composer/vendor/bin:"* ]]; then
	echo -e "\n-> Adding Composer's ~/AppData/Roaming/Composer/vendor/bin to your PATH"
	add_comment_to_profile
	echo "export PATH=~/AppData/Roaming/Composer/vendor/bin:\$PATH # include Composer global binaries" >> $profile_script
fi

source $profile_script

## INSTALL COMPOSER
if [[ ! -z $(which composer) ]] ; then
	echo -e "\n-------------------------------------------------------"
	echo -e "\n-> PHP Composer already installed."

else
	echo -e "\n-------------------------------------------------------"
	echo -e "\n-> Installing PHP Composer"

	source $profile_script

	echo -e "\n-> Downloading and installing Composer\n"
	curl -#Lk https://getcomposer.org/installer | php -- --install-dir=$HOME/bin
	echo -e "\n-> Renaming composer.phar to composer"
	mv ~/bin/composer.phar ~/bin/composer

	echo -e "\n-> Composer: Installing Laravel globally...\n"
	composer global require "laravel/installer=~1.1"
fi

if [[ $profile_uncommented -eq 0 ]] ; then
	echo " " >> $profile_script
	echo "# End Yoobee Changes" >> $profile_script
fi

echo -e "\n-------------------------------------------------------"
echo -e '\x1B[0;37;44m\n\n-> WEB DEVELOPMENT CONFIGURATOR COMPLETE!\n\x1B[0m'

echo -e "\n\x1B[0;32m-> The file $profile_script has been MODIFIED. Take a look at it.\x1B[0m\n"

if [[ ! -z $proxy_password ]] && [[ $yoobee_lab == [Nn] ]]; then
	echo -e "\n\x1B[0;32mInternet access from the command line is not enabled by default."
	echo -e "When on the Yoobee student network, in order to access the internet, run:\n"
	echo -e "\x1B[1;34m  yoobeeproxy\x1B[0;32m\n"
	echo -e "in each terminal window you need internet access from.\x1B[0m"
fi

echo -e "\x1B[41;97m\n***********************************************"
echo -e              "*    CLOSE ALL YOUR TERMINAL WINDOWS, NOW!    *"
echo -e              "***********************************************\x1B[0;m\n"
echo -e "\n$full_name, you have a nice day.\n"
